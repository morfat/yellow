from django.db import models
from django.utils import timezone

#for key creation and activation
import hashlib
import random

#email sending
from django.core.mail import send_mail
from yellow.settings import DEFAULT_FROM_EMAIL


class Interest(models.Model): #why the user is using our service
    name=models.CharField(max_length=100)
    
    def __str__(self):
        return self.name
    
    
    
class YellowUser(models.Model):
    email_address=models.EmailField(unique=True)
    full_name=models.CharField(max_length=100)
    gender=models.CharField(max_length=1,choices=(('M','Male'),('F','Female'),))
    date_of_birth=models.DateField()
    password=models.CharField(max_length=20,blank=True,null=True)
    is_verified=models.BooleanField(default=False)
    interests=models.ManyToManyField(Interest)
    
    date_created=models.DateTimeField(default=timezone.now)
    
    def __str__(self):
        return self.full_name
    
    @classmethod
    def set_password(cls,email_address,password):
        return cls.objects.filter(email_address=email_address).update(password=password)
    
    
    
class AccountConfirmation(models.Model):
    yellow_user=models.ForeignKey(YellowUser)
    activation_code=models.CharField(max_length=100)
    is_used=models.BooleanField(default=False)
    
    date_created=models.DateTimeField(default=timezone.now)
    
    def __str__(self):
        return self.activateion_code
    
    
    @classmethod
    def create(cls,user):
        #create confirmation code
        salt=hashlib.sha1(str(random.random()).encode('utf-8')).hexdigest()[:5]
        activation_code=hashlib.sha1((salt+user.email_address).encode('utf-8')).hexdigest()
        return cls.objects.create(yellow_user=user,activation_code=activation_code)
            
    
    def send_confirmation_mail(self,user):
        email_subject='Account Confirmtion'
        email_body='Thank you %s .Please click this link http://localhost:8000/yellow/email-confirm/%s \
                   to activate your account.'%(user.full_name,self.activation_code)
        send_mail(email_subject,email_body,DEFAULT_FROM_EMAIL,[user.email_address])
     
     
    @classmethod   
    def confirm_email(cls,activation_code):
        try:
            ac=cls.objects.get(activation_code=activation_code,is_used=False)
            ac.is_used=True
            ac.save()
            return ac
        except:
            return None
        
            
        
            

        
       
        
    
    
    
    
    
    
    
    