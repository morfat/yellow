from .models import AccountConfirmation
from .models import YellowUser

from django import forms

class RegisterForm(forms.ModelForm):
    class Meta:
        model=YellowUser
        exclude=['password','is_verified','date_created']
        

class SetPasswordForm(forms.Form):
    password=forms.CharField(widget=forms.PasswordInput())
    confirm_password=forms.CharField(widget=forms.PasswordInput())
    
    def clean(self): 
        data=self.cleaned_data
        
        if data['password'] != data['confirm_password']:
            self._errors['password']=['Passwords Do not Match']
            del data['password']
        return data

class LoginForm(forms.Form):
    email_address=forms.CharField()
    password=forms.CharField(widget=forms.PasswordInput())
    