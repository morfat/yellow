from django.test import TestCase

# Create your tests here.
from .models import Interest,YellowUser


class InterestTest(TestCase):
    #interest should exist before system starts
    def test_interest_exists(self):
        self.assertGreaterEqual(Interest.objects.all().count(),1)
        
        
class YellowUserTest(TestCase):
    def test_email_unique(self):
        error=False
        try:
            interest=Interest.objects.create(name='Test')
            
            yu=YellowUser.objects.create(email_address='test@test.com',
                                         full_name='Test',gender='M',
                                         date_of_birth='2015-01-01')
            yu2=YellowUser.objects.create(email_address='test@test.com',
                                         full_name='Test',gender='M',
                                         date_of_birth='2015-01-01')
        except:
            error=True
            
        self.assertTrue(error)
        
            
        
            
    
           
        