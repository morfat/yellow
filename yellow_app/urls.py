
from django.conf.urls import url
from .views import Register,ConfirmationEmailSent,EmailConfirm
from .views import SetPassword,Login





urlpatterns = [
    url(r'^register/$', Register.as_view(), name='register'),
    url(r'^confirmation-email-sent/$', ConfirmationEmailSent.as_view(), name='confirmation_email_sent'),
    url(r'^email-confirm/(?P<activation_code>\w+)/$', EmailConfirm.as_view(), name='email_confirm'),
    url(r'^set-password/$', SetPassword.as_view(), name='set_password'),
    url(r'^login/$', Login.as_view(), name='login'),
]
