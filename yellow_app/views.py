from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.views.generic import View
from django.contrib import messages
from django.http import HttpResponseRedirect

from .forms import RegisterForm,SetPasswordForm,LoginForm

from .models import AccountConfirmation,YellowUser

from .models import YellowUser 



class Register(View):
    template_name='yellow_app/register.html'
    form=RegisterForm()
    
    def get(self,request,*args,**kwargs):
        return render(request,self.template_name,{'form':self.form})
    
    def post(self,request,*args,**kwargs):
        self.form=RegisterForm(request.POST)
        if self.form.is_valid():
            yellow_user=self.form.save()
            ac=AccountConfirmation.create(yellow_user)
            ac.send_confirmation_mail(yellow_user)
            messages.add_message(request,messages.INFO,'We have Sent you a confirmation email. ')
            return HttpResponseRedirect(reverse('yellow:confirmation_email_sent'))
        return render(request,self.template_name,{'form':self.form})
    
        

class ConfirmationEmailSent(View):
    template_name='yellow_app/confirm_email_sent.html'
    def get(self,request,*args,**kwargs):
        return render(request,self.template_name)
    
class EmailConfirm(View):
    template_name='yellow_app/email_confirm.html'
    def get(self,request,*args,**kwargs):
        email_confirmed=AccountConfirmation.confirm_email(self.kwargs['activation_code'])
        if email_confirmed:
            #set email in session
            request.session['email']=email_confirmed.yellow_user.email_address
            return HttpResponseRedirect(reverse('yellow:set_password'))
        else:
            messages.add_message(request,messages.INFO,'Confirmation Email Code is Invalid')
        return render(request,self.template_name)
    
    
    
class SetPassword(View):
    template_name='yellow_app/set_password.html'
    form=SetPasswordForm()
    
    def get(self,request,*args,**kwargs):
        return render(request,self.template_name,{'form':self.form})
    
    def post(self,request,*args,**kwargs):
        self.form=SetPasswordForm(request.POST)
        if self.form.is_valid():
            email_address=request.session['email']
            password_set=YellowUser.set_password(email_address,
                                                 self.form.cleaned_data['password'])
            if password_set:
                return HttpResponseRedirect(reverse('yellow:login'))
            else:
                messages.add_message(request,messages.INFO,'Setting Password Failed')
        return render(request,self.template_name,{'form':self.form})
    
  
class Login(View):
    template_name='yellow_app/login.html'
    form=LoginForm()
    
    def get(self,request,*args,**kwargs):
        return render(request,self.template_name,{'form':self.form})
    
    def post(self,request,*args,**kwargs):
        self.form=LoginForm(request.POST)
        if self.form.is_valid():
            print ("logged in")
        return render(request,self.template_name,{'form':self.form})
    
    

    