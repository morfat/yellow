# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yellow_app', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='accountconfirmation',
            name='confirm_link',
        ),
        migrations.AddField(
            model_name='accountconfirmation',
            name='activation_code',
            field=models.CharField(max_length=100, default='1'),
            preserve_default=False,
        ),
    ]
