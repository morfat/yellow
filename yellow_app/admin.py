from django.contrib import admin
from .models import AccountConfirmation,Interest,YellowUser


admin.site.register(Interest)
admin.site.register(YellowUser)
admin.site.register(AccountConfirmation)